package imt.fila1.clecleath.tmbd.pages

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.CompoundButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.ToggleButton
import coil.load
import imt.fila1.clecleath.tmbd.R
import imt.fila1.clecleath.tmbd.models.Movie
import imt.fila1.clecleath.tmbd.services.MovieService

class MovieDetailActivity : AppCompatActivity() {
    lateinit var movieService : MovieService
    lateinit var currentMovie : Movie
    companion object{
        const val INTENT_PARAM_ID = "intent_param_id"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)

    }

    override fun onStart() {
        super.onStart()
        movieService = MovieService(this)
        val id = intent.getIntExtra(INTENT_PARAM_ID,-1)
        movieService.getMovieById(id,success = {
            runOnUiThread() {
                currentMovie = it
                findViewById<TextView>(R.id.movieTitle).text = currentMovie.title
                findViewById<TextView>(R.id.rateText).text = currentMovie.averageVote.toString()
                findViewById<TextView>(R.id.overviewText).text = currentMovie.overview
                findViewById<TextView>(R.id.castText).text = currentMovie.cast
                findViewById<ImageView>(R.id.imageView3).load("https://image.tmdb.org/t/p/original/"+currentMovie.backdropUrl)
                findViewById<ToggleButton>(R.id.favorite).isChecked = currentMovie.favorite

            }
        }, failure = {})
 //       findViewById<ToggleButton>(R.id.favorite).setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener())

    }
}