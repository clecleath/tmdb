package imt.fila1.clecleath.tmbd.services

import android.content.Context
import android.os.Debug
import imt.fila1.clecleath.tmbd.TmdbApplication
import imt.fila1.clecleath.tmbd.database.AppDatabase
import imt.fila1.clecleath.tmbd.models.MockDataSource
import imt.fila1.clecleath.tmbd.models.Movie
import imt.fila1.clecleath.tmbd.models.daos.MovieDao
import imt.fila1.clecleath.tmbd.models.transformers.movieDtoToMovie
import imt.fila1.clecleath.tmbd.models.transformers.movieListDtoToMoviesList
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class MovieService (context: Context){
    private val dataSource = MockDataSource()
    private lateinit var db: AppDatabase
    private lateinit var movieDao: MovieDao
    /*fun getMovieById(id:Int, success: (movie: Movie?) -> Unit, failure: () -> Unit) {
        success(dataSource.movies.find { it.id == id })
    }*/
    init {

        db = (context.applicationContext as TmdbApplication).getDatabase()

        movieDao = db.movieDao()
    }

    fun getMovieById(id:Int, success: (movie: Movie) -> Unit, failure: () -> Unit) {
        CoroutineScope(
            Dispatchers.IO
        ).launch {
            val response = ApiClient.movieInterface.movieDetailById(id)
            if (response.isSuccessful && response.body() != null) {
                val content = response.body()
                content?.let {
                    var movie = movieDtoToMovie(it)
                    movie.favorite = movieDao.getById(movie.id) != null
                    success(movie)
                }
            }
        }


    }

    fun getLastestMovies(
        success: (movies: List<Movie>) -> Unit, failure: () -> Unit) {
        CoroutineScope(
            Dispatchers.IO
        ).launch {
            val response = ApiClient.movieInterface.getLastestMovies()

            if (response.isSuccessful && response.body() != null) {
                val content = response.body()
                content?.let {
                    success(movieListDtoToMoviesList(it))
                }
            }
        }
    }
}