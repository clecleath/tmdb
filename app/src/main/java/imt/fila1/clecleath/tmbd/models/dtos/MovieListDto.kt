package imt.fila1.clecleath.tmbd.models.dtos

data class MovieListDto (
    val page: Int,
    val results:List<MovieListResultDto>,
    val total_pages:Int,
    val total_results:Int
    )