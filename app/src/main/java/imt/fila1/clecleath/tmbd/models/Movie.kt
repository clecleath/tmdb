package imt.fila1.clecleath.tmbd.models

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize
@Parcelize @Entity
data class Movie (
    @PrimaryKey var id : Int,
    //@ColumnInfo(name="poster_url")
    var posterUrl:String,
    //@ColumnInfo(name="backdrop_url")
    var backdropUrl:String,
    //@ColumnInfo(name="title")
    var title:String,
    //@ColumnInfo(name="average_vote")
    var averageVote:Float,
    //@ColumnInfo(name="votes_number")
    var votesNumber:Int,
    //@ColumnInfo(name="overview")
    var overview:String,
    //@ColumnInfo(name="cast")
    var cast:String,
    //@ColumnInfo(name="date")
    var date:String,
    ) : Parcelable {
        @Ignore
        var favorite :Boolean = false;
    }