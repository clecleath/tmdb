package imt.fila1.clecleath.tmbd.models.dtos

data class spokenLanguageDto(
    val english_name:String,
    val iso_639_1:String,
    val name:String,
)