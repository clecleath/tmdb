package imt.fila1.clecleath.tmbd.services

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import imt.fila1.clecleath.tmbd.services.interfaces.MovieInterface
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

object ApiClient {

    private val gson: Gson by lazy{
            GsonBuilder().setLenient().create()
        }

    class ApiKeyInterceptor : Interceptor {
        override fun intercept(chain: Interceptor.Chain): Response {
            val original: Request = chain.request()
            val originalHttpUrl = original.url

            val url = originalHttpUrl.newBuilder()
                .addQueryParameter("api_key", "74a4b573182d5dee6b4565cd5f152916")
                .build()

            val requestBuilder: Request.Builder = original.newBuilder()
                .url(url)

            val request = requestBuilder.build()
            return chain.proceed(request)
        }
    }
    private val httpClient: OkHttpClient by lazy {
        OkHttpClient.Builder()
            .addInterceptor(ApiKeyInterceptor())
            .build()
    }
    private val retrofit: Retrofit by lazy{
        Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/")
            .client(httpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    public val movieInterface: MovieInterface by lazy{
        retrofit.create(MovieInterface::class.java)
    }

    /*fun getService() : MovieInterface {
        return retrofit.create(MovieInterface::class.java)
    }*/
}

