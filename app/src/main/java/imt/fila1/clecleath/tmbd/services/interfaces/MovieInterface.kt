package imt.fila1.clecleath.tmbd.services.interfaces

import imt.fila1.clecleath.tmbd.models.dtos.MovieDetailDto
import imt.fila1.clecleath.tmbd.models.dtos.MovieListDto
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

public interface MovieInterface {
       @GET("movie/{id}")
       suspend fun movieDetailById(@Path("id") id: Int): Response<MovieDetailDto>
       @GET("movie/now_playing")
       suspend fun getLastestMovies() : Response<MovieListDto>
}