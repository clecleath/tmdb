package imt.fila1.clecleath.tmbd

import android.app.Application
import androidx.room.Room
import imt.fila1.clecleath.tmbd.database.AppDatabase

class TmdbApplication : Application() {
    private lateinit var db:AppDatabase

    override fun onCreate() {
        super.onCreate()

        db = Room.databaseBuilder(
            this,
            AppDatabase::class.java,
            "my-database"
        ).build()
    }


    fun getDatabase():AppDatabase {
        return db
    }

}