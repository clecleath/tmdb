package imt.fila1.clecleath.tmbd.models.daos

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import imt.fila1.clecleath.tmbd.models.Movie

@Dao
interface MovieDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addFavorite(vararg movie: Movie)

    @Query("select * from movie")
    fun getAll(): List<Movie>

    @Query("select * from movie where id=:id")
    fun getById(id:Int):Movie?

    @Delete
    fun removeFavorit(movie:Movie)
}