package imt.fila1.clecleath.tmbd.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import imt.fila1.clecleath.tmbd.models.Movie
import imt.fila1.clecleath.tmbd.models.daos.MovieDao

@Database(entities = [Movie::class], version = 2)
abstract class AppDatabase:RoomDatabase() {
    abstract fun movieDao() : MovieDao
}