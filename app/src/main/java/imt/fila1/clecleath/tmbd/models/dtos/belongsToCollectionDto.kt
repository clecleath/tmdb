package imt.fila1.clecleath.tmbd.models.dtos

data class belongsToCollectionDto (
    val id:Int,
    val name:String,
    val poster_path:String,
    val backdrop_path:String,
        )