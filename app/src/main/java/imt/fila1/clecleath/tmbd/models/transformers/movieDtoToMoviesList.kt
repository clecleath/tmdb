package imt.fila1.clecleath.tmbd.models.transformers

import imt.fila1.clecleath.tmbd.models.Movie
import imt.fila1.clecleath.tmbd.models.dtos.MovieDetailDto
import imt.fila1.clecleath.tmbd.models.dtos.MovieListDto

fun movieListDtoToMoviesList(latestMovieDto : MovieListDto):List<Movie> {
    val movies = latestMovieDto.results.map {
        Movie(
            id=it.id,
            posterUrl = it.poster_path,
            title = it.title,
            averageVote = it.vote_average.toFloat(),
            votesNumber = it.vote_count,
            overview = it.overview,
            date = it.release_date,
            cast = "",
            backdropUrl = it.backdrop_path,
        )

    }
    return movies
}

fun movieDtoToMovie(movieDetailDto :MovieDetailDto ):Movie {
    return Movie(
        movieDetailDto.id,
        movieDetailDto.poster_path,
        movieDetailDto.backdrop_path,
        movieDetailDto.title,
        movieDetailDto.vote_average,
        movieDetailDto.vote_count,
        movieDetailDto.overview,
        movieDetailDto.release_date,
        movieDetailDto.release_date,
    );

}
